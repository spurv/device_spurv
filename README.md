## Android

    mkdir aosp; cd aosp

Install `repo` as per https://source.android.com/setup/build/downloading

    repo init -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r59
    git clone https://gitlab.collabora.com/spurv/android_manifest.git .repo/local_manifests/
    repo sync -dc -j<threads>
    . build/envsetup.sh
    lunch spurv_x86_64-eng
    make -j<threads>