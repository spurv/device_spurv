#
# Copyright 2014 2019-2020 Collabora Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true

TARGET_BOARD_PLATFORM := spurv_x86_64

TARGET_ARCH := x86_64
TARGET_ARCH_VARIANT := silvermont
TARGET_CPU_ABI := x86_64
TARGET_CPU_ABI_LIST_32_BIT := x86
TARGET_2ND_ARCH := x86
TARGET_2ND_CPU_ABI := x86
TARGET_2ND_CPU_ABI2 := x86
TARGET_2ND_ARCH_VARIANT := silvermont
TARGET_2ND_CPU_VARIANT := silvermont
TARGET_TRANSLATE_2ND_ARCH := false

TARGET_USES_64_BIT_BINDER := true
TARGET_COPY_OUT_VENDOR := vendor
TARGET_NO_RECOVERY := true
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true

WITH_DEXPREOPT := true

BOARD_USES_VENDORIMAGE := true
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 100663296 # 96MB
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1610612736 # 1.5GB
BOARD_USERDATAIMAGE_PARTITION_SIZE := 576716800
BOARD_FLASH_BLOCK_SIZE := 512
BOARD_HAVE_BLUETOOTH := false
BOARD_USES_GENERIC_AUDIO := true
BOARD_GPU_DRIVERS := i915

VSYNC_EVENT_PHASE_OFFSET_NS := 0
USE_OPENGL_RENDERER := true
USE_CAMERA_STUB := true
USE_IIO_SENSOR_HAL := true
USE_IIO_ACTIVITY_RECOGNITION_HAL := true
NO_IIO_EVENTS := true

BOARD_SEPOLICY_DIRS += \
        device/spurv/spurv_x86_64/sepolicy
#        system/bt/vendor_libs/linux/sepolicy

BOARD_VNDK_VERSION := current